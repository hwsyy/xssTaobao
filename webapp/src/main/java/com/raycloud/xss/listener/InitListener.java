package com.raycloud.xss.listener;

import com.raycloud.xss.websocket.MyServer;
import java.net.UnknownHostException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Description:
 * User: ouzhouyou@raycloud.com
 * Date: 14-1-8
 * Time: 上午11:54
 * Version: 1.0
 */
public class InitListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        try {
            MyServer socketServer = new MyServer(8887);
            event.getServletContext().setAttribute("ws", socketServer);
            socketServer.start();
            System.out.println("WebSocketServer端口:8887");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void contextDestroyed(ServletContextEvent event) {

    }
}
